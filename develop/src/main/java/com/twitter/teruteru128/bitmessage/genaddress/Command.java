package com.twitter.teruteru128.bitmessage.genaddress;

public enum Command {
    GENERATE, VALIDATE, VERSION
}
