package com.twitter.teruteru128.bitmessage;

/**
 * SearchRangeFactory
 */
public interface SearchRangeFactory {

  SearchRange getInstance();
  
}
